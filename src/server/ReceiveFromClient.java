/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import core.Decrypt;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Keval Sanghvi
 */
public class ReceiveFromClient extends Thread {
    // Default Fields and References
    Socket socket;
    
    // Parameterized Constructor
    ReceiveFromClient(Socket socket) {
        this.socket = socket;
    }
    
    @Override
    public void run() {
        try {
            DataInputStream dis = new DataInputStream(socket.getInputStream());
            while(true) {
                String input = dis.readUTF();
                String decryptedInput = Decrypt.decrypt(input);
                if(decryptedInput.contains("code:")) {
                    String secretCode = decryptedInput.replace("code:", "");
                    if(secretCode.equals(Server.getSecretCode())) {
                        new SendToClient(socket, "Auth Success!").start();
                    } else {
                        new SendToClient(socket, "Auth Failure!").start();
                    }
                } else if(decryptedInput.contains("orderId:")) {
                    String patternString = "(orderId:)(.*?)(,)";
                    Pattern pattern = Pattern.compile(patternString);
                    Matcher matcher = pattern.matcher(decryptedInput);
                    if(matcher.find()) {
                        Server.orderSockets.put(matcher.group(2), socket);
                    }
                    OrderExtractor orderExtractor = new OrderExtractor(decryptedInput);
                }
            }
        } catch(IOException e) {
            System.out.println("An Error Occured At Server Side! " + e);
        }
    }
}