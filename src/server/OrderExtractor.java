package server;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import pojo.Order;
import pojo.OrderItem;

/**
 *
 * @author Marmik
 */
public class OrderExtractor {
    // Fields and References
    private final String message;
    private final Order order;

    // Parameterized Constructor 
    public OrderExtractor(String message) {
        this.message = message;
        this.order = new Order();
        this.findOrderId();
        this.findTableNumber();
        this.findOrderItems();
        Server.orders.add(this.order);
        Server.newOrder();
    }
    
    // Getters
    public Order getOrder() {
        return this.order;
    }
    
    // Methods (Extracting variables with Regex)
    public void findOrderId() {
        String patternString = "(orderId:)(.*?)(,)";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(this.message);
        if(matcher.find()) {
            this.order.setOrderId(matcher.group(2));
        }
    }
    
    public void findTableNumber() {
        String patternString = "(tableNumber:)(.*?)(,)";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(this.message);
        if(matcher.find()) {
            this.order.setTableNumber(matcher.group(2));
        }
    }
        
    public void findOrderItems() {
        String patternString = "\\{(Item:)(.*?)(,)(AdditionalDetails:)(.*?)(,)(Quantity:)(.*?)(,)\\}";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(this.message);
        while(matcher.find()) {
            OrderItem orderItem = new OrderItem();
            orderItem.setItem(matcher.group(2));
            orderItem.setAdditionalDetails(matcher.group(5));
            orderItem.setQuantity(matcher.group(8));
            this.order.setOrderItems(orderItem);
        }       
    }
}