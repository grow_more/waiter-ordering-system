/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import pojo.Order;

/**
 *
 * @author Keval Sanghvi
 */
public class Server {
    // PORT NUMBER
    private static final int PORT = 4567;
    
    // Secret Code
    private static final String SECRET_CODE = "kevalMarmik";
    
    // ArrayList for storing orders
    static final ArrayList<Order> orders = new ArrayList<>();
    
    // ServerUIFrame
    static ServerUIFrame ui;
    
    // HashMap for storing all users in database
    static final HashMap<String, Socket> orderSockets = new HashMap<>();

    // Constructor
    Server() {
        try {
            ServerSocket server = new ServerSocket(PORT);
            Server.ui = new ServerUIFrame(server);
            java.awt.EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    Server.ui.setVisible(true);
                }
            });
            
            while(true) {
                Socket socket = server.accept();
                new ReceiveFromClient(socket).start();
            }           
        } catch(IOException e) {
            System.out.println("An Error Occured At Server Side! " + e);
        }
    }
    
    public static void newOrder() {
        Server.ui.orderTable();
    }
    
    public static void main(String[] args) {
       new Server();
    }
    
    public static String getSecretCode() {
        return SECRET_CODE;
    }
}
