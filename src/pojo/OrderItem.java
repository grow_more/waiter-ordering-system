package pojo;

/**
 *
 * @author Marmik
 */
public class OrderItem {
    // Fields
    private String item;
    private String additionalDetails;
    private String quantity;
    
    // Default Constructor
    public OrderItem() {
    }
    
    // Parameterized Constructor
    public OrderItem(String item, String additionalDetails, String quantity) {
        this.item = item;
        this.additionalDetails = additionalDetails;
        this.quantity = quantity;
    }
    
    // Getters
    public String getItem() {
        return this.item;
    }
    
    public String getAdditionalDetails() {
        return this.additionalDetails;
    }
    
    public String getQuantity() {
        return this.quantity;
    }
    
    // Setters
    public void setItem(String item) {
        this.item = item;
    }
    
    public void setAdditionalDetails(String additionalDetails) {
        this.additionalDetails = additionalDetails;
    }
    
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
    
    // Method to get details
    public StringBuilder getDetails() {
        StringBuilder result = new StringBuilder();
        result.append("{Item:").append(this.item).append(",AdditionalDetails:").append(this.additionalDetails).append(",Quantity:").append(this.quantity).append(",}");
        return result;
    }
    
    // Method to display in popup
    public String displayString() {
        if(this.additionalDetails.isEmpty()) {
            return "Item: " + this.item + ", Quantity: " + this.quantity;
        }
        return "Item: " + this.item + ", Additional Details: " + this.additionalDetails + ", Quantity: " + this.quantity;
    }
}
