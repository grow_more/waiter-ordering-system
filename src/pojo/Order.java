package pojo;

import java.util.ArrayList;

/**
 *
 * @author Marmik
 */
public class Order {
    // Fields and References
    private String orderId;
    private String orderStatus;
    private String tableNumber;
    private final ArrayList<OrderItem> orderItems;
    
    // Default Constructor
    public Order() {
        // Initializing ArrayList
        this.orderItems = new ArrayList<>();
    }
    
    // Parameterized Constructor
    public Order(String orderId, String orderStatus, String tableNumber) {
        this.orderId = orderId;
        this.orderStatus = orderStatus;
        this.tableNumber = tableNumber;
        
        // Initializing ArrayList
        this.orderItems = new ArrayList<>();
    }

    // Getters
    public String getOrderId() {
        return this.orderId;
    }
    
    public String getOrderStatus() {
        return this.orderStatus;
    }
    
    public String getTableNumber() {
        return this.tableNumber;
    }
    
    public ArrayList<OrderItem> getOrderItems() {
        return this.orderItems;
    }
    
    // Setters
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }
    
    public void setTableNumber(String tableNumber) {
        this.tableNumber = tableNumber;
    }
    
    public void setOrderItems(OrderItem orderItem) {
        this.orderItems.add(orderItem);
    }
    
    // toString method
    @Override
    public String toString() {
        return "Order: " + this.orderId;
    }
    
    // Method to get details about order
    public StringBuilder getDetails() {
        StringBuilder result = new StringBuilder();
        result.append("{orderId:").append(this.orderId).append(",tableNumber:").append(this.tableNumber).append(",}");
        result.append("[");
        for(OrderItem i: this.orderItems) {
            result.append(i.getDetails());
        }
        result.append("]");
        return result;
    }
}
