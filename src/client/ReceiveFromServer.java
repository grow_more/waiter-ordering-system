/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import core.Decrypt;
import javax.swing.JOptionPane;

/**
 *
 * @author Keval Sanghvi
 */
public class ReceiveFromServer extends Thread {
    // Default Fields and References
    Socket socket;
    ClientAuthFrame clientFrame;
    ClientUIFrame ui;
    
    // Parameterized Constructor
    ReceiveFromServer(Socket socket, ClientAuthFrame clientFrame) {
        this.socket = socket;
        this.clientFrame = clientFrame;
    }
    
    @Override
    public void run() {
        try {
            DataInputStream dis = new DataInputStream(socket.getInputStream());
            while(true) {
                String input = dis.readUTF();
                String decryptedInput = Decrypt.decrypt(input);
                if(decryptedInput.equals("Auth Success!")) {
                    this.ui = clientFrame.authSuccess();
                } else if(decryptedInput.equals("Auth Failure!")) {
                    clientFrame.authFailure();
                } else {
                    JOptionPane.showMessageDialog(this.ui, "Order with ID: " + decryptedInput + " is ready!");  
                    Client.orders.removeIf(i -> i.getOrderId().equals(decryptedInput));
                    this.ui.orderTable();
                }
            }
        } catch(IOException e) {
            System.out.println("An Error Occured At Client Side! " + e);
        }
    }
}
