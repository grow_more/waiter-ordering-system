/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import pojo.Order;

/**
 *
 * @author Keval Sanghvi
 */
public class Client {
    // Default Fields and References
    Socket socket;
    
    // list of pending orders
    static ArrayList<Order> orders;
    
    // PORT NUMBER
    private static final int PORT = 4567;

    // Parameterized Constructor
    Client() {
        try {
            socket = new Socket("localhost", PORT);
            orders = new ArrayList<>();
            java.awt.EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    new ClientAuthFrame(socket).setVisible(true);
                }
            });
        } catch(IOException e) {
            System.out.println("An Error Occured At Client Side! " + e);
        }
    }
    
    public static void main(String[] args) {
        new Client();
    }
    
    // Method that returns the socket of this object.
    public Socket getSocket() {
        return this.socket;
    }
    
    // Method that removes fulfilled orders from arraylist
    public static void fulfilledOrders() {
        Iterator itr = Client.orders.iterator();
        while(itr.hasNext()) {
            Order i = (Order)itr.next();
            if(i.getOrderStatus().equals("Fulfilled")) {
                itr.remove();
            }
        }
    }
}
